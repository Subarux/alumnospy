import Coleccion
import mysql_connector

class Alumno:    
    def __init__(self):
        self.__rut="" #Privado con guion
        self.__nombre=""
        self.Apellido="" #publico
        self.Evaluacion1=""
        self.Evaluacion2=""
        self.Evaluacion3=""
    
    def getRut(self):
        return self.__rut
    
    def setRut(self,valor):
        if(len(valor)!=0 or valor==None):
            self.__rut=valor
        else:
            raise Exception("Debe ingresar el Rut")    
    rut=property(getRut,setRut)  
    
    def getNombre(self):
        return self.__nombre
    
    def setNombre(self,valor):
        if(len(valor)!=0 or valor==None):
            self.__nombre=valor
        else:
            raise Exception("Debe ingresar el Nombre")  
    
    nombre=property(getNombre,setNombre)
    
    def DatosAlumno(self):
        datos=[self.__rut,self.__nombre,self.Apellido,self.Evaluacion1,self.Evaluacion2,self.Evaluacion3]
        return datos

    def Crear(self):
        #coleccion.listaclientes.append(self)
        sql = "INSERT INTO cliente (rut, nombre, apellido, evaluacion1, evaluacion2, evaluacion3) VALUES (%s, %s, %s, %s, %s)"
        val = [(self.__rut, self.__nombre,self.Apellido,self.Evaluacion1,self.Evaluacion2,self.Evaluacion3)]
        mysql_connector.sql_insert(sql,val)
        return
        
    def Update(self,rut,nombre,apellido,evaluacion1,evaluacion2,evaluacion3):
        sql = "UPDATE ALUMNO SET"
        sql +=" NOMBRE ='" + nombre + "',"
        sql +=" APELLIDO = '" + apellido + "',"
        sql +=" EVALUACION1 ='" + evaluacion1 + "',"
        sql +=" EVALUACION2 ='" + evaluacion2 + "',"
        sql +=" EVALUACION3 ='" + evaluacion3 + "' "
        sql +=" WHERE RUT ='" + rut + "'"
        mysql_connector.sql_update(sql)
        return
    
    def Eliminar(self,rutKill):
        try:
            sql = "DELETE FROM alumno WHERE rut = '"+rutKill+"'"
            mysql_connector.sql_delete(sql)
        except:
            print('No se ha podido eliminar el rut')
        return
    
    def ListaAlumnos(self):
        return Coleccion.listaAlumnos
    
    def Buscar(self,rutalumno):
        sql = "SELECT * FROM ALUMNO WHERE rut='" + rutalumno + "'"
        datos=mysql_connector.sql_select(sql)
        
        if(len(datos)>0):
            for col in datos:
                self.__rut=col[0]
                self.__nombre=col[1]
                self.apellido=col[2]
                self.evaluacion1=col[3]
                self.evaluacion2=col[4]
                self.evaluacion3=col[5]
                return self
        else:
            return None
    
    '''def ClientesXComuna(self): 
        sql = "SELECT comuna, count(1) as cantidad FROM cliente GROUP BY comuna"
        datos=mysql_connector.sql_select(sql)
        return datos

    def ClientesXEstado(self): 
        sql = "SELECT estado, count(1) as cantidad FROM cliente GROUP BY estado"
        datos=mysql_connector.sql_select(sql)
        return datos

    def ClientesXSexo(self): 
        sql = "SELECT sexo, count(1) as cantidad FROM cliente GROUP BY sexo"
        datos=mysql_connector.sql_select(sql)
        return datos
    '''
       
        
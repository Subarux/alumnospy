# INSTALAR MYSQL CONNECTOR DESDE PIP
# python -m pip install mysql-connector

import sqlite3

mydb = sqlite3.connect(
  host="localhost",
  user="root",
  passwd="mysql",
  database="trabajador"
)

#mycursor = mydb.cursor()

def sql_select(query):
    mycursor = mydb.cursor()
    mycursor.execute(query)
    myresult = mycursor.fetchall()
    mycursor.close()
    return myresult

def sql_insert(sql, val):
    """
    Ejemplo:
    sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
    val = [
    ('Peter', 'Lowstreet 4'),
    ('Amy', 'Apple st 652')
    ]
    """
    mycursor = mydb.cursor()
    mycursor.executemany(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "was inserted.")
    mycursor.close()

def sql_delete(query):
    mycursor = mydb.cursor()
    mycursor.execute(query)
    mydb.commit()
    print(mycursor.rowcount, "record(s) deleted")
    mycursor.close()

def sql_update(query):
    mycursor = mydb.cursor()
    mycursor.execute(query)
    mydb.commit()
    print(mycursor.rowcount, "Datos Actualizados")
    mycursor.close()
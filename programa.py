'''Rut, Nombre Completo, Sexo [M/F], Comuna [Cerrillos, El Monte, Maipú],
Estado del Servicio [Operativo/No Operativo]'''
import os
import Alumnos as miAlumno

alumno=[]

comunas=["Cerrillos","El Monte","Maipú"]
servicio=["Operativo","No Operativo"]

def Ingresar():
    try:
        alu=miAlumno.Alumno()
        print("Ingrese Rut")
        alu.rut=input()
        
        print("Ingrese Nombre")
        alu.nombre=input()
        
        print("Ingrese Apellido")
        alu.apellido=input()
        
        print("Ingrese la primera evaluacion")
        alu.evaluacion1=input()
        
        print("Ingrese la segunda evaluacion")
        alu.evaluacion2=input()
        
        print("Ingrese la tercera evaluacion")
        alu.evaluacion3=input()
        
        '''while True:
            print("Ingrese Sexo [M/F]")
            sex=input().upper()
            if sex=='M':
                cli.sexo="Masculino"
                break
            elif sex=='F':
                cli.sexo="Femenino"
                break
            else:
                print("Opción no válida")
        
        while True:
            print("Ingrese comuna Comuna [1. Cerrillos, 2. El Monte, 3. Maipú]")
            com=int(input())
            if(com<1 or com>3):
                print("Comuna nó valida")
            else:
                cli.comuna=comunas[com-1]
                break'''
        
        alu.estado="Operativo"
        alu.Crear()
        #print(cli.DatosCliente())
        #clientes.append(cli)
        
        print("Alumno agregado con éxito!!!...")
        
        
    except Exception as error:
        print('ERROR: ',error)
      
def Editar():
    try:
        print("Ingrese el rut del Alumno")
        r=input()
        
        #ClienteData=[]
        cli=miAlumno.Alumno()
        AlumnoData = alu.Buscar(r)
        #print(type(ClienteData))
        #print(ClienteData[0])
        if(AlumnoData==None):
            print("alumno no encontrado")
            return
        else:
            print(AlumnoData.DatosAlumno())
            print("Ingrese Nombre")
            nombre=input()
            
            '''
            while True:
                print("Ingrese Sexo [M/F]")
                sex=input().upper()
                if sex=='M':
                    sexo="Masculino"
                    break
                elif sex=='F':
                    sexo="Femenino"
                    break
                else:
                    print("Opción no válida")
                
            while True:
                print("Ingrese comuna Comuna [1. Cerrillos, 2. El Monte, 3. Maipú]")
                com=int(input())
                if(com<1 or com>3):
                    print("Comuna nó valida")
                else:
                    comuna=comunas[com-1]
                    break
                        
            while True:
                print("Ingrese estado del servicio 1. Operativo / 2. No operativo")
                srv=int(input())
                if srv==1:
                    estado="Operativo"
                    break
                elif srv==2:
                    estado="No Operativo"
                    break
                else:
                    print("Estado no válido")
            ''' 
            #print(ClienteData.DatosCliente()) 
            #Updatear en la BD
            AlumnoData.Update(r,nombre,apellido,evaluacion1,evaluacion2,evaluacion3)
            print("OK!!")      
                                        
    except Exception as error:
        print('ERROR: ',error)
    
def Eliminar():
    print("Ingrese Rut a eliminar")
    r=input()
    if len(r)==0 or r==None:
        print("No Ingreso rut")
        Pausa()
    else:
        alu=miAlumno.Alumno()
        alu.Eliminar(r)
        
def TotalAlumnos():
    alu=miAlumno.Alumno()
    print('COMUNA\tCANTIDAD')
    for row in alu.ClientesXComuna():
        print(f"{row[0]}\t{row[1]}")

def ListadoAlumnos():
    alu=miAlumno.Alumno()
    print('ESTADO\tCANTIDAD')
    for row in alu.ClientesXEstado():
        print(f"{row[0]}\t{row[1]}")


def ClientesPorSexo():
    alu=miAlumno.Alumno()
    print('SEXO\tCANTIDAD')
    for row in alu.ClientesXSexo():
        print(f"{row[0]}\t{row[1]}")
          
def Pausa():
    print("Presione una tecla para continuar...")
    input()
    os.system ("cls") 


#MAIN
os.system ("cls") 
while True:
   
    print("1. Ingresar Alumno")
    print("2. Editar Estado del Servicio")
    print("3. Eliminar Alumno")
    print("4. Total Clientes por Comuna")
    print("5. Listado de Clientes por Estado de Servicio")
    print("6. Clientes por Sexo")
    print("7. Salir")    
    optext=input("Ingrese una opción: ")
    if len(optext)==0:
        print("Debe Ingresar una opción")
    else:
        op=int(optext)
    
        if(op<1 or op>7):
            print("opción no válida")
        if(op==1):
            Ingresar()
            Pausa()
        if(op==2):
            Editar()
            
        if(op==3):
            Eliminar()
           
        if(op==4):
            TotalAlumnos()
            Pausa()
        if(op==5):
            ListadoAlumnos()
            Pausa()
        if(op==6):
            ClientesPorSexo()
            Pausa()
            
        if(op==7):
            print("FIN DEL PROGRAMA....")
            break
 
    
